#!/bin/bash

if [ -z "$SSH_ORIGINAL_COMMAND" ]; then
  echo "SSH_ORIGINAL_COMMAND missing, not running via command= in the authorized_keys file?"
  exit 1
fi

# Filter out most special chars.
FILTERED_COMMAND=$(echo "$SSH_ORIGINAL_COMMAND" | sed 's/[^a-zA-Z0-9_]//g')

if [ -z "$FILTERED_COMMAND" -o "$SSH_ORIGINAL_COMMAND" != "$FILTERED_COMMAND"  ]; then
  echo "SSH_ORIGINAL_COMMAND denied, invalid chars detected."
  exit 1
fi

# Lookup config file.
if [ -f 'ssh_whitelist.conf' ]; then
  config_file=ssh_whitelist.conf
else
  if [ -f '.ssh_whitelist.conf' ]; then
    config_file=.ssh_whitelist.conf
  else
    if [ -f '/etc/ssh/whitelist.conf' ]; then
      config_file=/etc/ssh/whitelist.conf
    else
      echo "No ssh_whitelist.conf found"
      exit 1
    fi
  fi
fi

# Lookup the real command, config is compatible with /etc/nagios/nrpe_local.cfg
real_command=$(egrep "^command\[$FILTERED_COMMAND\]=" $config_file | cut -d\= -f2-)

if [ -z "$real_command" ]; then
  echo "unknown command: $FILTERED_COMMAND"
  exit 3;
fi

# Execute the command
$real_command
