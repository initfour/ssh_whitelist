SSH Command Whitelist
=====================


A script to only allow executing a set of pre-defined commands via ssh based on a public key.

To call this script you can use the command argument in SSH's authorized_keys file.
The user should not have a valid password on the account.
```
command="/usr/local/bin/ssh_whitelist.sh",no-port-forwarding,no-X11-forwarding,no-agent-forwarding ssh-rsa ...<key>... user@host
```


Config example
--------------

The config for this uses the style of the Nagios NRPE config.
Looks for ssh_whitelist.conf in the current directory and /etc/ssh/whitelist.conf.

```
command[some_command_name]=/usr/bin/some_command

# e.g.
command[check_users]=/usr/lib/nagios/plugins/check_users -w 5 -c 10
```


Icinga
------

This script was designed to replace NRPE and be used from Icinga2. Below are some snippits that can help to get started.

```
template Host "generic-debian-host" {
  import "generic-host"

  vars.os = "Linux"
}


template Service "generic-by-ssh-service" {
  import "generic-service"

  check_command = "by_ssh"

  vars.by_ssh_logname = "nagios"
  vars.by_ssh_timeout = 80
}

# LOAD
apply Service "load-by-ssh-check"  {
  import "generic-by-ssh-service"

  display_name = "LOAD"

  # NRPE style
  vars.by_ssh_command = "check_load"

  # For graphite template
  vars.check_command = "load"

  assign where (host.address || host.address6) && host.vars.os == "Linux"
}


```


Ansible
-------

When using Ansible the snipit below can be used to add a line to the whitelist.
The extra test step is added to allow tweaking the lines parameters on each server. I've been unable to find a way to let the lineinfile module add the line when missing and not update an existing one.

```
  - name: copy ssh_whitelist script.
    copy: src=ssh_whitelist/ssh_whitelist.sh dest=/usr/local/bin/ssh_whitelist.sh mode=0755 owner=root group=root

  - name: initialize a clean ssh_whitelist config.
    copy: src=ssh_whitelist.conf dest=/etc/ssh/whitelist.conf mode=0640 owner=root group=nagios

  - name: Test for line
    shell: grep "command\[check_apt" /etc/ssh/whitelist.conf
    changed_when: false
    failed_when: false
    register: test_grep

  - name: Add apt line to ssh_whitelist.conf.
    lineinfile:
      dest=/etc/ssh/whitelist.conf
      regexp="^command\\[check_apt"
      line="command[check_apt]=/usr/lib/nagios/plugins/check_apt"
    when: test_grep.stdout is defined and test_grep.stdout == ""

```

