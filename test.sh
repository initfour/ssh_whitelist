
echo "=== Scenario's that should work:"

# OK:
SSH_ORIGINAL_COMMAND="check_load" ./ssh_whitelist.sh
SSH_ORIGINAL_COMMAND="check_disk" ./ssh_whitelist.sh

echo "=== The rest should fail:"

# Should fail:
SSH_ORIGINAL_COMMAND="" ./ssh_whitelist.sh
SSH_ORIGINAL_COMMAND=".*" ./ssh_whitelist.sh
SSH_ORIGINAL_COMMAND="-" ./ssh_whitelist.sh
SSH_ORIGINAL_COMMAND="\'cat /etc/motd\'" ./ssh_whitelist.sh
SSH_ORIGINAL_COMMAND="\`cat /etc/motd\`" ./ssh_whitelist.sh
SSH_ORIGINAL_COMMAND="/bin/true" ./ssh_whitelist.sh
SSH_ORIGINAL_COMMAND="\$(/bin/true)" ./ssh_whitelist.sh
SSH_ORIGINAL_COMMAND="\$(date)" ./ssh_whitelist.sh
